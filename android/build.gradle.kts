import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

plugins {
	alias(libs.plugins.android.application)
	alias(libs.plugins.kotlin.android)
	alias(libs.plugins.kotlin.serialization)
	alias(libs.plugins.google.ksp)
	alias(libs.plugins.kotlin.compose)
}

@Throws(IOException::class)
fun String.execute(): Process = Runtime.getRuntime().exec(this)

@Throws(IOException::class)
fun Process.getText(): String =
	org.codehaus.groovy.runtime.IOGroovyMethods.getText(
		BufferedReader(
			InputStreamReader(
				inputStream
			)
		)
	).also {
		org.codehaus.groovy.runtime.ProcessGroovyMethods.closeStreams(this)
	}

val versionMajor = 2
val versionMinor = 5
val versionPatch = 0
val versionBuild = System.getenv("CI_PIPELINE_IID")?.toIntOrNull() ?: 0

val computedVersionName by lazy { "$versionMajor.$versionMinor.$versionPatch" + if (versionBuild > 0) "+$versionBuild" else "" }

// Version code: S VVVVV MMMMMMM PPPPP IIIIIIIIIIIIII (32-bit integer)
// S (x1):  Sign bit. Must always be 0 for an android version code
// V (x5):  Major version. Up to 32, which should be enough (especially since we are still on 0)
//          This might not work for apps that follow proper semantic versioning, but who does that?
// M (x7):  Minor version. Up to 128, which should be enough
// P (x5):  Patch version. Up to 32, which should be enough for these
// I (x14): Pipeline ID bits. Allows a total of 16384 pipeline runs.
//          I'm simply guessing that that'll be enough
//
// This implementation assumes that these maximum numbers will never be reached.
// If they are reached, the version codes "bleed over" into the next range,
// so this should technically still produce valid, higher versions, but the format will be broken.
val computedVersionCode by lazy {
	var bits = 0
	bits = (bits shl 5) or versionMajor
	bits = (bits shl 7) or versionMinor
	bits = (bits shl 5) or versionPatch
	bits = (bits shl 14) or versionBuild
	bits
}

@Throws(IOException::class)
fun getCommitCount(): String = "git rev-list --count HEAD".execute().getText().trim()

android {
	compileSdk = 35
	defaultConfig {
		applicationId = "app.shosetsu.android.sy"
		minSdk = 22
		targetSdk = 35
		versionCode = computedVersionCode
		versionName = computedVersionName
		testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
		multiDexEnabled = true

		setProperty("archivesBaseName", rootProject.name)
		vectorDrawables {
			useSupportLibrary = true
		}
	}

	buildFeatures {
		viewBinding = true
		compose = true
		buildConfig = true
	}

	composeOptions {
		kotlinCompilerExtensionVersion = "1.5.7"
	}

	/*
	splits {
		abi {
			isEnable = true

			isUniversalApk = true
		}
	}
	 */


	buildTypes {
		named("release") {
			isMinifyEnabled = true
			isShrinkResources = true
			proguardFiles(
				getDefaultProguardFile("proguard-android-optimize.txt"),
				"proguard-rules.pro"
			)
			versionNameSuffix = ""
			multiDexEnabled = true
		}
		named("debug") {
			versionNameSuffix = "-${getCommitCount()}"
			applicationIdSuffix = ".debug"
			isDebuggable = true
			isMinifyEnabled = true
			isShrinkResources = true
			proguardFiles(
				getDefaultProguardFile("proguard-android-optimize.txt"),
				"proguard-rules.pro"
			)
		}
	}
	compileOptions {
		sourceCompatibility = JavaVersion.VERSION_17
		targetCompatibility = JavaVersion.VERSION_17
		isCoreLibraryDesugaringEnabled = true
	}
	kotlinOptions {
		jvmTarget = JavaVersion.VERSION_17.toString()
		freeCompilerArgs = freeCompilerArgs + "-Xjvm-default=all-compatibility"
	}

	lint {
		disable.addAll(listOf("MissingTranslation", "ExtraTranslation"))
		abortOnError = false
	}
	namespace = "app.shosetsu.android"
	packaging {
		resources {
			excludes += "/META-INF/{AL2.0,LGPL2.1}"
		}
	}
}

ksp {
	arg("room.schemaLocation", "$projectDir/schemas")
}

//TODO Fix application variant naming
/*
android.applicationVariants.forEach { variant ->
	variant.outputs.all {
		val v = this as com.android.build.gradle.internal.api.ApkVariantOutputImpl
		val appName = "shosetsu"
		val versionName = variant.versionName
		val versionCode = variant.versionCode
		val flavorName = variant.flavorName
		val buildType = variant.buildType.name
		val variantName = variant.name
		val gitCount = getCommitCount()

		outputFileName = "${appName}-" +
				if (buildType == "debug" && flavorName.toString() == "standard") {
					gitCount
				} else {
					versionName
				} + ".apk"
	}
}
 */

dependencies {
	implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

	implementation(platform(libs.kotlin.bom))

	// Androidx
	implementation(libs.androidx.work.runtime)
	implementation(libs.androidx.work.runtime.ktx)
	implementation(libs.androidx.appcompat)
	implementation(libs.androidx.annotation)
	implementation(libs.androidx.core.ktx)
	implementation(libs.androidx.collection.ktx)
	implementation(libs.androidx.core.splashscreen)
	implementation(libs.androidx.coordinatorlayout)
	implementation(libs.androidx.window)
	implementation(libs.androidx.compose.material3.wsc)
	implementation(libs.androidx.lifecycle.runtime.ktx)
	implementation(libs.androidx.activity.compose)

	implementation(platform(libs.androidx.compose.bom))
	androidTestImplementation(platform(libs.androidx.compose.bom))

	androidTestImplementation(libs.androidx.compose.ui.testjunit4)
	androidTestImplementation(platform(libs.androidx.compose.bom))

	debugImplementation(libs.androidx.compose.ui.tooling)
	debugImplementation(libs.androidx.compose.ui.testmanifest)

	// - Life Cycle

	implementation(libs.androidx.lifecycle.viewmodel.ktx)
	implementation(libs.androidx.lifecycle.viewmodel.compose)
	implementation(libs.androidx.lifecycle.viewmodel.savedstate)
	implementation(libs.androidx.lifecycle.runtime.ktx)


	// Test classes
	testImplementation(libs.junit)
	testImplementation(libs.androidx.test.ext.junit)
	androidTestImplementation(libs.androidx.test.runner)
	androidTestImplementation(libs.androidx.test.espresso.core)


	// Core libraries
	implementation(libs.luaj.jse)
	implementation(libs.shosetsuorg.klib)
	implementation(libs.jsoup)

	// Image loading
	implementation(libs.coil.compose)

	// Time control
	implementation(libs.joda.time)

	// Cloud flare calculator
	//implementation("com.zhkrb.cloudflare-scrape-android:scrape-webview:0.0.3")

	// Network
	implementation(libs.okhttp)

	// Kotlin libraries
	implementation(kotlin("stdlib-jdk8"))
	//implementation(kotlin("reflect"))

	implementation(libs.kotlinx.coroutines.android)

	implementation(libs.kotlinx.collections.immutable)

	// Conductor
	/*
	val conductorVersion = "3.1.5"
	fun conductor(module: String, version: String = conductorVersion) =
		"com.bluelinelabs:$module:$version"

	implementation(conductor("conductor"))
	implementation(conductor("conductor-androidx-transition"))
	implementation(conductor("conductor-archlifecycle"))
	 */

	// Room

	implementation(libs.androidx.room.runtime)
	annotationProcessor(libs.androidx.room.compiler)
	ksp(libs.androidx.room.compiler)
	implementation(libs.androidx.room.ktx)
	implementation(libs.androidx.room.paging)

	// Guava cache
	implementation(libs.google.guava)

	// kode-in

	implementation(libs.kodein.di)
	implementation(libs.kodein.di.jvm)
	implementation(libs.kodein.di.framework.android.core)
	implementation(libs.kodein.di.framework.androidx)
	implementation(libs.kodein.di.framework.androidx.viewmodel)
	implementation(libs.kodein.di.framework.androidx.viewmodel.savedstate)

	// KTX

	implementation(libs.kotlinx.coroutines.jdk8)

	// KTX - Serialization
	implementation(libs.kotlinx.serialization.json)

	// Kryo
	implementation(libs.kryo)

	// Roomigrant
	/*val enableRoomigrant = false

	val roomigrantVersion = "0.3.4"
	implementation("com.github.MatrixDev.Roomigrant:RoomigrantLib:$roomigrantVersion")
	if (enableRoomigrant) {
		kapt("com.github.MatrixDev.Roomigrant:RoomigrantCompiler:$roomigrantVersion")
	}*/

	// Compose

	implementation(platform(libs.androidx.compose.bom))
	implementation(libs.androidx.compose.runtime)
	implementation(libs.androidx.compose.ui)
	implementation(libs.androidx.compose.ui.tooling)
	implementation(libs.androidx.compose.ui.graphics)
	implementation(libs.androidx.compose.ui.tooling.preview)
	implementation(libs.androidx.compose.material3)
    implementation(libs.androidx.material.icons.extended)
	implementation(libs.androidx.compose.foundation)
	implementation(libs.androidx.compose.animation)
	implementation(libs.androidx.compose.animation.graphics)
	implementation(libs.androidx.compose.animation.core)

	// - accompanist

	implementation(libs.google.accompanist.appcompat.theme)
	implementation(libs.google.accompanist.webview)
	implementation(libs.google.accompanist.placeholder.material)
	implementation(libs.google.accompanist.pager.indicators)
	implementation(libs.google.accompanist.permissions)
	implementation(libs.google.accompanist.systemuicontroller)

	//- Integration with observables
	implementation(libs.androidx.compose.runtime.livedata)

	// MDC Adapter
	implementation(libs.google.accompanist.themeadapter.material)
	implementation(libs.google.accompanist.themeadapter.material3)

	implementation(libs.androidx.activity)
	implementation(libs.androidx.activity.ktx)
	implementation(libs.androidx.activity.compose)

	implementation(libs.numberpicker)

	// QR Code
	implementation(libs.qrcode)

	// - paging

	implementation(libs.androidx.paging.runtime)
	implementation(libs.androidx.paging.compose)

	implementation(kotlin("reflect"))

	implementation(libs.androidx.navigation.compose)

	coreLibraryDesugaring(libs.desugar)

	implementation(libs.bottomsheetdialog)
}